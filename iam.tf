resource "oci_identity_compartment" "compartment1" {
  compartment_id = "${var.compartment_id}"
  name           = "XYZ-Prod"
  description    = "compartment for XYZ-Prod"
  enable_delete  = true                              // true will cause this compartment to be deleted when running `terrafrom destroy`
}

data "oci_identity_compartments" "compartments1" {
  compartment_id = "${oci_identity_compartment.compartment1.compartment_id}"

  filter {
    name   = "name"
    values = ["XYZ-Prod"]
  }
}

output "compartment1" {
 
  value = "${data.oci_identity_compartments.compartments1.compartments}"
  
}


resource "oci_identity_compartment" "compartment2" {
  compartment_id = "${var.compartment_id}"
  name           = "XYZ-Share"
  description    = "compartment for XYZ-Share"
  enable_delete  = true                              // true will cause this compartment to be deleted when running `terrafrom destroy`
}

data "oci_identity_compartments" "compartments2" { 
  compartment_id = "${oci_identity_compartment.compartment2.compartment_id}"

  filter {
    name   = "name"
    values = ["XYZ-Share"]
  }
}

output "compartment2" {
 
   value = "${data.oci_identity_compartments.compartments2.compartments}"
}



/*
 * This example file shows how to create a user, add an api key, define auth tokens and customer secret keys.
 */

resource "oci_identity_user" "user1" { 
  compartment_id = "${var.compartment_id}"
  name           = "prodadm@xyz.com"
  description    = "user1 created by terraform"
}

data "oci_identity_users" "users1" {
  compartment_id = "${oci_identity_user.user1.compartment_id}"

  filter {
    name   = "name"
    values = ["prodadm@xyz.com"]
  }
}

output "users1" {
  value = "${data.oci_identity_users.users1.users}"
}

resource "oci_identity_ui_password" "password1" {
  user_id = "${oci_identity_user.user1.id}"
}

data "oci_identity_ui_password" "password1" {
  user_id = "${oci_identity_user.user1.id}"
}

output "user-password1" {
  sensitive = false
  value     = "${oci_identity_ui_password.password1.password}"
}

resource "oci_identity_user" "user2" { 
  compartment_id = "${var.compartment_id}"
  name           = "shareadm@xyz.com"
  description    = "user2 created by terraform"
}

data "oci_identity_users" "users2" {
  compartment_id = "${oci_identity_user.user2.compartment_id}"

  filter {
    name   = "name"
    values = ["shareadm@xyz.com"]
  }
}

output "users2" {
  value = "${data.oci_identity_users.users2.users}"
}

resource "oci_identity_ui_password" "password2" {
  user_id = "${oci_identity_user.user2.id}"
}

data "oci_identity_ui_password" "password2" {
  user_id = "${oci_identity_user.user2.id}"
}

output "user-password2" {
  sensitive = false
  value     = "${oci_identity_ui_password.password2.password}"
}

/*
 * This example file shows how to create a group and add a user to it. 
 */
resource "oci_identity_group" "group1" {
  name           = "xyz-grp-Admin"
  description    = "group1 created by terraform"
  compartment_id = "${var.compartment_id}"
}

resource "oci_identity_user_group_membership" "user-group-mem1" {
  compartment_id = "${var.compartment_id}"
  user_id        = "${oci_identity_user.user1.id}"
  group_id       = "${oci_identity_group.group1.id}"
}

data "oci_identity_groups" "groups1" {
  compartment_id = "${oci_identity_group.group1.compartment_id}"

  filter {
    name   = "name"
    values = ["xyz-grp-Admin"]
  }
}

output "groups1" {
  value = "${data.oci_identity_groups.groups1.groups}"
}

resource "oci_identity_group" "group2" {
  name           = "xyz-grp-Shared"
  description    = "group2 created by terraform"
  compartment_id = "${var.compartment_id}"
}

resource "oci_identity_user_group_membership" "user-group-mem2" {
  compartment_id = "${var.compartment_id}"
  user_id        = "${oci_identity_user.user2.id}"
  group_id       = "${oci_identity_group.group2.id}"
}

data "oci_identity_groups" "groups2" {
  compartment_id = "${oci_identity_group.group2.compartment_id}"

  filter {
    name   = "name"
    values = ["xyz-grp-Shared"]
  }
}

output "groups2" {
  value = "${data.oci_identity_groups.groups2.groups}"
}


resource "oci_identity_policy" "policy1" {
  name           = "XYZ-admin-policy1"
  description    = "policy created by terraform"
  compartment_id = "${data.oci_identity_compartments.compartments1.compartments.0.id}"

  statements = ["Allow group ${oci_identity_group.group1.name} to manage all-resources in compartment ${data.oci_identity_compartments.compartments1.compartments.0.name}"]
}

data "oci_identity_policies" "policies1" {
compartment_id = "${data.oci_identity_compartments.compartments1.compartments.0.id}"

  filter {
    name   = "name"
    values = ["XYZ-admin-policy1"]
  }
}

output "policy1" {
  value = "${data.oci_identity_policies.policies1.policies}"
}

resource "oci_identity_policy" "policy2" {
  name           = "XYZ-admin-policy2"
  description    = "policy created by terraform"
  compartment_id = "${data.oci_identity_compartments.compartments2.compartments.0.id}"

  statements = ["Allow group ${oci_identity_group.group2.name} to manage all-resources in compartment ${data.oci_identity_compartments.compartments2.compartments.0.name}"]
}

data "oci_identity_policies" "policies2" {
compartment_id = "${data.oci_identity_compartments.compartments2.compartments.0.id}"

  filter {
    name   = "name"
    values = ["XYZ-admin-policy2"]
  }
}

output "policy2" {
  value = "${data.oci_identity_policies.policies1.policies}"
}
