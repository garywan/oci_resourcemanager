// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

variable "region" {}
variable "ssh_public_key" {}
variable "ssh_private_key" {}
variable "compartment_id" {}
variable "vcn01_cidr" {
  default = "10.51.0.0/16"
}
variable "sub01_name" {
  default = "SN-Prod-DB-01"
}
variable "sub01_cidr" {
  default = "10.51.1.0/24"
}
variable "vcn02_cidr" {
  default = "10.52.0.0/16"
}
variable "sub02_name" {
  default = "SN-Prod-FTP-01"
}
variable "sub02_cidr" {
  default = "10.52.1.0/24"
}