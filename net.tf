resource "oci_core_vcn" "vcn01" {
  cidr_block     = "${var.vcn01_cidr}"
  dns_label      = "vcn01"
  compartment_id = "${oci_identity_compartment.compartment1.id}"
  display_name   = "XYZ-Prod-VCN01"
}

resource "oci_core_vcn" "vcn02" {
  cidr_block     = "${var.vcn02_cidr}"
  dns_label      = "vcn02"
  compartment_id = "${oci_identity_compartment.compartment2.id}"
  display_name   = "XYZ-Share-VCN02"
}


# Peer vcn01 to vcn02. You need one peering gateway on each VCN per peering connection.

resource "oci_core_local_peering_gateway" "test_local_peering_gateway_01" {
  #Required
  compartment_id = "${oci_identity_compartment.compartment1.compartment_id}"
  vcn_id         = "${oci_core_vcn.vcn01.id}"

  #Optional
  display_name = "localPeeringGateway1"
  peer_id      = "${oci_core_local_peering_gateway.test_local_peering_gateway_02.id}"
}


resource "oci_core_local_peering_gateway" "test_local_peering_gateway_02" {
  #Required
  compartment_id = "${oci_identity_compartment.compartment2.compartment_id}"
  vcn_id         = "${oci_core_vcn.vcn02.id}"

  #Optional
  display_name = "localPeeringGateway2"
}


data "oci_core_local_peering_gateways" "test_local_peering_gateways1" {
  #Required
  compartment_id = "${oci_identity_compartment.compartment1.compartment_id}"
  vcn_id         = "${oci_core_vcn.vcn01.id}"
}

data "oci_core_local_peering_gateways" "test_local_peering_gateways2" {
  #Required
  compartment_id = "${oci_identity_compartment.compartment2.compartment_id}"
  vcn_id         = "${oci_core_vcn.vcn02.id}"
}

resource "oci_core_internet_gateway" "test_internet_gateway2" {
  compartment_id = "${oci_identity_compartment.compartment2.compartment_id}"
  display_name   = "XYZ-IG01"
  vcn_id         = "${oci_core_vcn.vcn02.id}"
}

resource "oci_core_route_table" "route_table1" {
  compartment_id = "${oci_identity_compartment.compartment1.compartment_id}"
  vcn_id         = "${oci_core_vcn.vcn01.id}"
  display_name   = "RT-VCN01-DB01"

   route_rules {
    destination       = "${var.vcn02_cidr}"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = "${oci_core_local_peering_gateway.test_local_peering_gateway_01.id}"
  }
}

resource "oci_core_default_route_table" "default_route_table2" {
  manage_default_resource_id = "${oci_core_vcn.vcn02.default_route_table_id}"
  display_name               = "RT-VCN02-FTP01_defaultRouteTable"

  route_rules {
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = "${oci_core_internet_gateway.test_internet_gateway2.id}"
  }
}

resource "oci_core_route_table" "route_table2" {
  compartment_id = "${oci_identity_compartment.compartment2.compartment_id}"
  vcn_id         = "${oci_core_vcn.vcn02.id}"
  display_name   = "RT-VCN02-FTP01"

  route_rules {
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = "${oci_core_internet_gateway.test_internet_gateway2.id}"
  }
   route_rules {
    destination       = "${var.vcn01_cidr}"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = "${oci_core_local_peering_gateway.test_local_peering_gateway_02.id}"
  }
}

resource "oci_core_default_security_list" "default_security_list1" {
  manage_default_resource_id = "${oci_core_vcn.vcn01.default_security_list_id}"
  display_name               = "SL-VCN01"

  // allow outbound tcp traffic on all ports
  egress_security_rules {
    destination = "0.0.0.0/0"
    protocol    = "6"
  }

  // allow outbound udp traffic on a port range
  egress_security_rules {
    destination = "0.0.0.0/0"
    protocol    = "17"        // udp
    stateless   = true

    udp_options {
      min = 319
      max = 320
    }
  }

  // allow inbound ssh traffic
  ingress_security_rules {
    protocol  = "6"         // tcp
    source    = "0.0.0.0/0"
    stateless = false

    tcp_options {
      min = 22
      max = 22
    }
  }
  
    // allow inbound ftp traffic
  ingress_security_rules {
    protocol  = "6"         // tcp
    source    = "0.0.0.0/0"
    stateless = false

    tcp_options {
      min = 21
      max = 21
    }
  }

  // allow inbound icmp traffic of a specific type
  ingress_security_rules {
    protocol  = 1
    source    = "0.0.0.0/0"
    stateless = true

    icmp_options {
      type = 3
      code = 4
    }
  }
}


resource "oci_core_default_security_list" "default_security_list2" {
  manage_default_resource_id = "${oci_core_vcn.vcn02.default_security_list_id}"
  display_name               = "SL-VCN02"

  // allow outbound tcp traffic on all ports
  egress_security_rules {
    destination = "0.0.0.0/0"
    protocol    = "6"
  }

  // allow outbound udp traffic on a port range
  egress_security_rules {
    destination = "0.0.0.0/0"
    protocol    = "17"        // udp
    stateless   = true

    udp_options {
      min = 319
      max = 320
    }
  }

  // allow inbound ssh traffic
  ingress_security_rules {
    protocol  = "6"         // tcp
    source    = "0.0.0.0/0"
    stateless = false

    tcp_options {
      min = 22
      max = 22
    }
  }
// allow inbound https traffic
  ingress_security_rules {
    protocol  = "6"         // tcp
    source    = "0.0.0.0/0"
    stateless = false

    tcp_options {
      min = 443
      max = 443
    }
  }
  // allow inbound icmp traffic of a specific type
  ingress_security_rules {
    protocol  = 1
    source    = "0.0.0.0/0"
    stateless = true

    icmp_options {
      type = 3
      code = 4
    }
  }
}

resource "oci_core_subnet" "subnet01" {
  cidr_block        = "${var.sub01_cidr}"
  display_name      = "${var.sub01_name}"
  dns_label         = "subnet01"
  compartment_id    = "${oci_identity_compartment.compartment1.compartment_id}"
  vcn_id            = "${oci_core_vcn.vcn01.id}"
  security_list_ids = ["${oci_core_vcn.vcn01.default_security_list_id}"]
  route_table_id    = "${oci_core_vcn.vcn01.default_route_table_id}"
  dhcp_options_id   = "${oci_core_vcn.vcn01.default_dhcp_options_id}"
}

resource "oci_core_subnet" "subnet02" {
  cidr_block        = "${var.sub02_cidr}"
  display_name      = "${var.sub02_name}"
  dns_label         = "subnet02"
  compartment_id    = "${oci_identity_compartment.compartment2.compartment_id}"
  vcn_id            = "${oci_core_vcn.vcn02.id}"
  security_list_ids = ["${oci_core_vcn.vcn02.default_security_list_id}"]
  route_table_id    = "${oci_core_vcn.vcn02.default_route_table_id}"
  dhcp_options_id   = "${oci_core_vcn.vcn02.default_dhcp_options_id}"
}